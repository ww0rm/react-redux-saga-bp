import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { reducers } from '../reducers';
import sagas from '../sagas/index';

const { logger } = require('redux-logger');

const saga = createSagaMiddleware();

let middleware = [];
middleware.push(saga);
middleware.push(logger);

const store = createStore(reducers, {}, applyMiddleware(...middleware));
saga.run(sagas);

export default store;