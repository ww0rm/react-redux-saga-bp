import { Provider } from 'react-redux'
import React from 'react';
import App from '../App';
import store from '../../config/store';

function Root() {
    return (
        <Provider store={store}>
            <App />
        </Provider>
    );
}

export default Root;
