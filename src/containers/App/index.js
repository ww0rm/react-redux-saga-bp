import React from 'react';
import { connect } from 'react-redux'
import './index.css';
import {Actions} from "../../actions/statement";

function App(props) {
  const handleButtonClick = () => {
      props.load();
  };

  return (
    <div className="App">
      <header className="App-header">
        <p style={{ display: props.loading ? 'block' : 'none' }}>Loading</p>
        <button onClick={handleButtonClick}>Click me</button>
        <ul>{
            props.records.map((item, key) => {
                return <li key={key}>{item.description}</li>;
            })
        }</ul>
      </header>
    </div>
  );
}

const mapStateToProps = state => {
    return { ...state.statementReducer }
};

const mapDispatchToProps = dispatch => {
    return {
        load: () => {
            dispatch({type: Actions.STATEMENT_REQUEST});
        }
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
