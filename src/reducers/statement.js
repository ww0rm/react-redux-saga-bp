import { Actions } from '../actions/statement';

const initialState = {
    loading: false,
    errorMessage: null,
    records: []
};

export const statementReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case Actions.STATEMENT_REQUEST:
            return { ...state, loading: true, records: []};

        case Actions.STATEMENT_SUCCESS:
            return { ...state, loading: false, records: payload };

        case Actions.STATEMENT_FAILURE:
            return { ...state, loading: false, errorMessage: payload };

        case Actions.STATEMENT_CLEAR_ERROR:
            return { ...state, errorMessage: null };

        default:
            return state
    }
}