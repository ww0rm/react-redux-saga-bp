import {combineReducers} from 'redux';
import { statementReducer } from './statement';

export const reducers = combineReducers({
    statementReducer: statementReducer
});