import { Actions } from '../actions/statement';
import { put, takeLatest } from 'redux-saga/effects';

export function* load() {
    yield takeLatest(Actions.STATEMENT_REQUEST, function* doLoad() {
        try {
            const data = yield fetch('https://api.myjson.com/bins/oivjj');
            const tickets = yield data.json();

            yield put({ type: Actions.STATEMENT_SUCCESS, payload: tickets });
        } catch (e) {
            console.error(e);
            yield put({ type: Actions.STATEMENT_FAILURE, payload: e.message });
        }
    });
}